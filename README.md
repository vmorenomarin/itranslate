# Instant Translate

Select text, press a shortcut and see the translation on a notification.

**Develop** by Daniel Mejia: danielmejia55@gmail.com. <br /> 
**Minor improvements** by Víctor Moreno: vmorenomarin@gmail.com.

Based on the original script on [Настройка Linux](https://habrahabr.ru/post/137215/)

## Installation

- First install `xsel`.
- Clone the repository: `git clone https://gitlab.com/vmorenomarin/itranslate.git`
- Go to the clonned repository: `cd itranslate`
- Execute the installation file: `chmod +x install & ./install`
- Go to the system settings and assign a shortcut for the command `itranslate`


## How works?

In your system settings find the shortcuts secction. Assign a key and use the command `itranslate` to invoke iTranslate. When you 
select a text press the assigned key and a notification will appears in your desktop enviroment with the text translated.

![iTranslate working](http://i.imgur.com/RGRioGc.png)
